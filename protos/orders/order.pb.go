// Code generated by protoc-gen-go. DO NOT EDIT.
// source: protos/orders/order.proto

package orders

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	timestamp "github.com/golang/protobuf/ptypes/timestamp"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

// User
type User struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Email                string   `protobuf:"bytes,3,opt,name=email,proto3" json:"email,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *User) Reset()         { *m = User{} }
func (m *User) String() string { return proto.CompactTextString(m) }
func (*User) ProtoMessage()    {}
func (*User) Descriptor() ([]byte, []int) {
	return fileDescriptor_6d26cbd64de0b17b, []int{0}
}

func (m *User) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_User.Unmarshal(m, b)
}
func (m *User) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_User.Marshal(b, m, deterministic)
}
func (m *User) XXX_Merge(src proto.Message) {
	xxx_messageInfo_User.Merge(m, src)
}
func (m *User) XXX_Size() int {
	return xxx_messageInfo_User.Size(m)
}
func (m *User) XXX_DiscardUnknown() {
	xxx_messageInfo_User.DiscardUnknown(m)
}

var xxx_messageInfo_User proto.InternalMessageInfo

func (m *User) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *User) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *User) GetEmail() string {
	if m != nil {
		return m.Email
	}
	return ""
}

// Product info
type ProductInfo struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Quantity             uint64   `protobuf:"varint,3,opt,name=quantity,proto3" json:"quantity,omitempty"`
	Price                float32  `protobuf:"fixed32,4,opt,name=price,proto3" json:"price,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ProductInfo) Reset()         { *m = ProductInfo{} }
func (m *ProductInfo) String() string { return proto.CompactTextString(m) }
func (*ProductInfo) ProtoMessage()    {}
func (*ProductInfo) Descriptor() ([]byte, []int) {
	return fileDescriptor_6d26cbd64de0b17b, []int{1}
}

func (m *ProductInfo) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ProductInfo.Unmarshal(m, b)
}
func (m *ProductInfo) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ProductInfo.Marshal(b, m, deterministic)
}
func (m *ProductInfo) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ProductInfo.Merge(m, src)
}
func (m *ProductInfo) XXX_Size() int {
	return xxx_messageInfo_ProductInfo.Size(m)
}
func (m *ProductInfo) XXX_DiscardUnknown() {
	xxx_messageInfo_ProductInfo.DiscardUnknown(m)
}

var xxx_messageInfo_ProductInfo proto.InternalMessageInfo

func (m *ProductInfo) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *ProductInfo) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *ProductInfo) GetQuantity() uint64 {
	if m != nil {
		return m.Quantity
	}
	return 0
}

func (m *ProductInfo) GetPrice() float32 {
	if m != nil {
		return m.Price
	}
	return 0
}

// Order
type Order struct {
	Id                   string               `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	ProductInfo          *ProductInfo         `protobuf:"bytes,2,opt,name=productInfo,proto3" json:"productInfo,omitempty"`
	User                 *User                `protobuf:"bytes,3,opt,name=user,proto3" json:"user,omitempty"`
	Date                 *timestamp.Timestamp `protobuf:"bytes,4,opt,name=date,proto3" json:"date,omitempty"`
	Owner                *User                `protobuf:"bytes,5,opt,name=owner,proto3" json:"owner,omitempty"`
	XXX_NoUnkeyedLiteral struct{}             `json:"-"`
	XXX_unrecognized     []byte               `json:"-"`
	XXX_sizecache        int32                `json:"-"`
}

func (m *Order) Reset()         { *m = Order{} }
func (m *Order) String() string { return proto.CompactTextString(m) }
func (*Order) ProtoMessage()    {}
func (*Order) Descriptor() ([]byte, []int) {
	return fileDescriptor_6d26cbd64de0b17b, []int{2}
}

func (m *Order) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Order.Unmarshal(m, b)
}
func (m *Order) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Order.Marshal(b, m, deterministic)
}
func (m *Order) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Order.Merge(m, src)
}
func (m *Order) XXX_Size() int {
	return xxx_messageInfo_Order.Size(m)
}
func (m *Order) XXX_DiscardUnknown() {
	xxx_messageInfo_Order.DiscardUnknown(m)
}

var xxx_messageInfo_Order proto.InternalMessageInfo

func (m *Order) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *Order) GetProductInfo() *ProductInfo {
	if m != nil {
		return m.ProductInfo
	}
	return nil
}

func (m *Order) GetUser() *User {
	if m != nil {
		return m.User
	}
	return nil
}

func (m *Order) GetDate() *timestamp.Timestamp {
	if m != nil {
		return m.Date
	}
	return nil
}

func (m *Order) GetOwner() *User {
	if m != nil {
		return m.Owner
	}
	return nil
}

// Get an Order by email request
type GetOrderRequest struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetOrderRequest) Reset()         { *m = GetOrderRequest{} }
func (m *GetOrderRequest) String() string { return proto.CompactTextString(m) }
func (*GetOrderRequest) ProtoMessage()    {}
func (*GetOrderRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_6d26cbd64de0b17b, []int{3}
}

func (m *GetOrderRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetOrderRequest.Unmarshal(m, b)
}
func (m *GetOrderRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetOrderRequest.Marshal(b, m, deterministic)
}
func (m *GetOrderRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetOrderRequest.Merge(m, src)
}
func (m *GetOrderRequest) XXX_Size() int {
	return xxx_messageInfo_GetOrderRequest.Size(m)
}
func (m *GetOrderRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_GetOrderRequest.DiscardUnknown(m)
}

var xxx_messageInfo_GetOrderRequest proto.InternalMessageInfo

func (m *GetOrderRequest) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

// Get an Order by email response
type GetOrderResponse struct {
	Order                *Order   `protobuf:"bytes,1,opt,name=Order,proto3" json:"Order,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetOrderResponse) Reset()         { *m = GetOrderResponse{} }
func (m *GetOrderResponse) String() string { return proto.CompactTextString(m) }
func (*GetOrderResponse) ProtoMessage()    {}
func (*GetOrderResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_6d26cbd64de0b17b, []int{4}
}

func (m *GetOrderResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetOrderResponse.Unmarshal(m, b)
}
func (m *GetOrderResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetOrderResponse.Marshal(b, m, deterministic)
}
func (m *GetOrderResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetOrderResponse.Merge(m, src)
}
func (m *GetOrderResponse) XXX_Size() int {
	return xxx_messageInfo_GetOrderResponse.Size(m)
}
func (m *GetOrderResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_GetOrderResponse.DiscardUnknown(m)
}

var xxx_messageInfo_GetOrderResponse proto.InternalMessageInfo

func (m *GetOrderResponse) GetOrder() *Order {
	if m != nil {
		return m.Order
	}
	return nil
}

type Patch struct {
	Op                   string   `protobuf:"bytes,1,opt,name=op,proto3" json:"op,omitempty"`
	Path                 string   `protobuf:"bytes,2,opt,name=path,proto3" json:"path,omitempty"`
	Value                string   `protobuf:"bytes,3,opt,name=value,proto3" json:"value,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Patch) Reset()         { *m = Patch{} }
func (m *Patch) String() string { return proto.CompactTextString(m) }
func (*Patch) ProtoMessage()    {}
func (*Patch) Descriptor() ([]byte, []int) {
	return fileDescriptor_6d26cbd64de0b17b, []int{5}
}

func (m *Patch) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Patch.Unmarshal(m, b)
}
func (m *Patch) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Patch.Marshal(b, m, deterministic)
}
func (m *Patch) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Patch.Merge(m, src)
}
func (m *Patch) XXX_Size() int {
	return xxx_messageInfo_Patch.Size(m)
}
func (m *Patch) XXX_DiscardUnknown() {
	xxx_messageInfo_Patch.DiscardUnknown(m)
}

var xxx_messageInfo_Patch proto.InternalMessageInfo

func (m *Patch) GetOp() string {
	if m != nil {
		return m.Op
	}
	return ""
}

func (m *Patch) GetPath() string {
	if m != nil {
		return m.Path
	}
	return ""
}

func (m *Patch) GetValue() string {
	if m != nil {
		return m.Value
	}
	return ""
}

// Update an Order request
type UpdateOrderRequest struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	JsonPatch            []*Patch `protobuf:"bytes,2,rep,name=jsonPatch,proto3" json:"jsonPatch,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *UpdateOrderRequest) Reset()         { *m = UpdateOrderRequest{} }
func (m *UpdateOrderRequest) String() string { return proto.CompactTextString(m) }
func (*UpdateOrderRequest) ProtoMessage()    {}
func (*UpdateOrderRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_6d26cbd64de0b17b, []int{6}
}

func (m *UpdateOrderRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UpdateOrderRequest.Unmarshal(m, b)
}
func (m *UpdateOrderRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UpdateOrderRequest.Marshal(b, m, deterministic)
}
func (m *UpdateOrderRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UpdateOrderRequest.Merge(m, src)
}
func (m *UpdateOrderRequest) XXX_Size() int {
	return xxx_messageInfo_UpdateOrderRequest.Size(m)
}
func (m *UpdateOrderRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_UpdateOrderRequest.DiscardUnknown(m)
}

var xxx_messageInfo_UpdateOrderRequest proto.InternalMessageInfo

func (m *UpdateOrderRequest) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *UpdateOrderRequest) GetJsonPatch() []*Patch {
	if m != nil {
		return m.JsonPatch
	}
	return nil
}

// Update an Order response
type UpdateOrderResponse struct {
	Result               string   `protobuf:"bytes,1,opt,name=result,proto3" json:"result,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *UpdateOrderResponse) Reset()         { *m = UpdateOrderResponse{} }
func (m *UpdateOrderResponse) String() string { return proto.CompactTextString(m) }
func (*UpdateOrderResponse) ProtoMessage()    {}
func (*UpdateOrderResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_6d26cbd64de0b17b, []int{7}
}

func (m *UpdateOrderResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UpdateOrderResponse.Unmarshal(m, b)
}
func (m *UpdateOrderResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UpdateOrderResponse.Marshal(b, m, deterministic)
}
func (m *UpdateOrderResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UpdateOrderResponse.Merge(m, src)
}
func (m *UpdateOrderResponse) XXX_Size() int {
	return xxx_messageInfo_UpdateOrderResponse.Size(m)
}
func (m *UpdateOrderResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_UpdateOrderResponse.DiscardUnknown(m)
}

var xxx_messageInfo_UpdateOrderResponse proto.InternalMessageInfo

func (m *UpdateOrderResponse) GetResult() string {
	if m != nil {
		return m.Result
	}
	return ""
}

// Insert an Order request
type InsertOrderRequest struct {
	Order                *Order   `protobuf:"bytes,1,opt,name=Order,proto3" json:"Order,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *InsertOrderRequest) Reset()         { *m = InsertOrderRequest{} }
func (m *InsertOrderRequest) String() string { return proto.CompactTextString(m) }
func (*InsertOrderRequest) ProtoMessage()    {}
func (*InsertOrderRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_6d26cbd64de0b17b, []int{8}
}

func (m *InsertOrderRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_InsertOrderRequest.Unmarshal(m, b)
}
func (m *InsertOrderRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_InsertOrderRequest.Marshal(b, m, deterministic)
}
func (m *InsertOrderRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_InsertOrderRequest.Merge(m, src)
}
func (m *InsertOrderRequest) XXX_Size() int {
	return xxx_messageInfo_InsertOrderRequest.Size(m)
}
func (m *InsertOrderRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_InsertOrderRequest.DiscardUnknown(m)
}

var xxx_messageInfo_InsertOrderRequest proto.InternalMessageInfo

func (m *InsertOrderRequest) GetOrder() *Order {
	if m != nil {
		return m.Order
	}
	return nil
}

// Insert an Order response
type InsertOrderResponse struct {
	Order                *Order   `protobuf:"bytes,1,opt,name=Order,proto3" json:"Order,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *InsertOrderResponse) Reset()         { *m = InsertOrderResponse{} }
func (m *InsertOrderResponse) String() string { return proto.CompactTextString(m) }
func (*InsertOrderResponse) ProtoMessage()    {}
func (*InsertOrderResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_6d26cbd64de0b17b, []int{9}
}

func (m *InsertOrderResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_InsertOrderResponse.Unmarshal(m, b)
}
func (m *InsertOrderResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_InsertOrderResponse.Marshal(b, m, deterministic)
}
func (m *InsertOrderResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_InsertOrderResponse.Merge(m, src)
}
func (m *InsertOrderResponse) XXX_Size() int {
	return xxx_messageInfo_InsertOrderResponse.Size(m)
}
func (m *InsertOrderResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_InsertOrderResponse.DiscardUnknown(m)
}

var xxx_messageInfo_InsertOrderResponse proto.InternalMessageInfo

func (m *InsertOrderResponse) GetOrder() *Order {
	if m != nil {
		return m.Order
	}
	return nil
}

func init() {
	proto.RegisterType((*User)(nil), "orders.User")
	proto.RegisterType((*ProductInfo)(nil), "orders.ProductInfo")
	proto.RegisterType((*Order)(nil), "orders.Order")
	proto.RegisterType((*GetOrderRequest)(nil), "orders.GetOrderRequest")
	proto.RegisterType((*GetOrderResponse)(nil), "orders.GetOrderResponse")
	proto.RegisterType((*Patch)(nil), "orders.Patch")
	proto.RegisterType((*UpdateOrderRequest)(nil), "orders.UpdateOrderRequest")
	proto.RegisterType((*UpdateOrderResponse)(nil), "orders.UpdateOrderResponse")
	proto.RegisterType((*InsertOrderRequest)(nil), "orders.InsertOrderRequest")
	proto.RegisterType((*InsertOrderResponse)(nil), "orders.InsertOrderResponse")
}

func init() { proto.RegisterFile("protos/orders/order.proto", fileDescriptor_6d26cbd64de0b17b) }

var fileDescriptor_6d26cbd64de0b17b = []byte{
	// 464 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x94, 0x52, 0xdd, 0x6e, 0x94, 0x40,
	0x14, 0x2e, 0x14, 0x36, 0xed, 0xa1, 0xfe, 0x64, 0xd6, 0x28, 0xe2, 0x85, 0xeb, 0x78, 0xd3, 0xc4,
	0xc8, 0x26, 0x18, 0x63, 0xf4, 0x46, 0xbd, 0xd2, 0x5e, 0x59, 0xd1, 0x3e, 0x00, 0x85, 0xd3, 0x16,
	0xb3, 0xcb, 0x4c, 0x67, 0x86, 0x1a, 0xdf, 0xcf, 0x37, 0xf1, 0x45, 0x0c, 0x67, 0x98, 0xc2, 0x96,
	0x8d, 0xa9, 0x57, 0x9c, 0x9f, 0x8f, 0xef, 0x9c, 0xef, 0x3b, 0x03, 0x8f, 0xa5, 0x12, 0x46, 0xe8,
	0xa5, 0x50, 0x15, 0xaa, 0xfe, 0x93, 0x52, 0x8d, 0xcd, 0x6c, 0x2d, 0x79, 0x7a, 0x2e, 0xc4, 0xf9,
	0x0a, 0x97, 0x54, 0x3d, 0x6d, 0xcf, 0x96, 0xa6, 0x5e, 0xa3, 0x36, 0xc5, 0x5a, 0x5a, 0x20, 0xff,
	0x00, 0xc1, 0x89, 0x46, 0xc5, 0xee, 0x82, 0x5f, 0x57, 0xb1, 0xb7, 0xf0, 0x0e, 0xf7, 0x73, 0xbf,
	0xae, 0x18, 0x83, 0xa0, 0x29, 0xd6, 0x18, 0xfb, 0x54, 0xa1, 0x98, 0x3d, 0x80, 0x10, 0xd7, 0x45,
	0xbd, 0x8a, 0x77, 0xa9, 0x68, 0x13, 0x5e, 0x42, 0x74, 0xac, 0x44, 0xd5, 0x96, 0xe6, 0xa8, 0x39,
	0x13, 0xb7, 0x22, 0x4a, 0x60, 0xef, 0xb2, 0x2d, 0x1a, 0x53, 0x9b, 0x5f, 0xc4, 0x15, 0xe4, 0xd7,
	0x79, 0x37, 0x44, 0xaa, 0xba, 0xc4, 0x38, 0x58, 0x78, 0x87, 0x7e, 0x6e, 0x13, 0xfe, 0xdb, 0x83,
	0xf0, 0x4b, 0x27, 0x69, 0xc2, 0xff, 0x1a, 0x22, 0x39, 0x8c, 0xa7, 0x31, 0x51, 0x36, 0x4f, 0xad,
	0xfe, 0x74, 0xb4, 0x59, 0x3e, 0xc6, 0xb1, 0x05, 0x04, 0xad, 0x46, 0x45, 0xe3, 0xa3, 0xec, 0xc0,
	0xe1, 0x3b, 0x2f, 0x72, 0xea, 0xb0, 0x14, 0x82, 0xaa, 0x30, 0x76, 0x8f, 0x28, 0x4b, 0x52, 0xeb,
	0x64, 0xea, 0x9c, 0x4c, 0xbf, 0x3b, 0x27, 0x73, 0xc2, 0x31, 0x0e, 0xa1, 0xf8, 0xd9, 0xa0, 0x8a,
	0xc3, 0x2d, 0x94, 0xb6, 0xc5, 0x9f, 0xc1, 0xbd, 0x4f, 0x68, 0x48, 0x48, 0x8e, 0x97, 0x2d, 0x6a,
	0x73, 0x53, 0x0f, 0x7f, 0x03, 0xf7, 0x07, 0x88, 0x96, 0xa2, 0xd1, 0xc8, 0x9e, 0xf7, 0xe2, 0x09,
	0x16, 0x65, 0x77, 0x1c, 0xb5, 0x45, 0xd9, 0x1e, 0xff, 0x08, 0xe1, 0x71, 0x61, 0xca, 0x8b, 0x8e,
	0x51, 0x48, 0xc7, 0x28, 0x64, 0x77, 0x01, 0x59, 0x98, 0x0b, 0x77, 0x81, 0x2e, 0xee, 0x5c, 0xbe,
	0x2a, 0x56, 0x2d, 0xba, 0x53, 0x52, 0xc2, 0xbf, 0x02, 0x3b, 0x91, 0x9d, 0x98, 0x7f, 0x6d, 0xc8,
	0x5e, 0xc0, 0xfe, 0x0f, 0x2d, 0x1a, 0x1a, 0x16, 0xfb, 0x8b, 0xdd, 0xf1, 0x46, 0x54, 0xcc, 0x87,
	0x3e, 0x7f, 0x09, 0xf3, 0x0d, 0xca, 0x5e, 0xd1, 0x43, 0x98, 0x29, 0xd4, 0xed, 0xca, 0xf4, 0xbc,
	0x7d, 0xc6, 0xdf, 0x02, 0x3b, 0x6a, 0x34, 0xaa, 0x4d, 0x8f, 0x6e, 0xa5, 0xff, 0x1d, 0xcc, 0x37,
	0x7e, 0xfd, 0x0f, 0xef, 0xb2, 0x3f, 0x1e, 0x1c, 0x50, 0xf4, 0x0d, 0xd5, 0x55, 0x5d, 0x22, 0x7b,
	0x0f, 0x7b, 0xee, 0x0a, 0xec, 0x91, 0xfb, 0xe5, 0xc6, 0xe9, 0x92, 0x78, 0xda, 0xb0, 0x43, 0xf9,
	0x0e, 0xfb, 0x0c, 0xd1, 0x48, 0x37, 0x4b, 0xae, 0x5f, 0xc3, 0xc4, 0xdf, 0xe4, 0xc9, 0xd6, 0xde,
	0x98, 0x69, 0xa4, 0x6b, 0x60, 0x9a, 0xfa, 0x34, 0x30, 0x6d, 0x31, 0x82, 0xef, 0x9c, 0xce, 0xe8,
	0xed, 0xbe, 0xfa, 0x1b, 0x00, 0x00, 0xff, 0xff, 0xc5, 0x7a, 0x06, 0x84, 0x38, 0x04, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// OrderServiceClient is the client API for OrderService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type OrderServiceClient interface {
	GetOrder(ctx context.Context, in *GetOrderRequest, opts ...grpc.CallOption) (*GetOrderResponse, error)
	UpdateOrder(ctx context.Context, in *UpdateOrderRequest, opts ...grpc.CallOption) (*UpdateOrderResponse, error)
	InsertOrder(ctx context.Context, in *InsertOrderRequest, opts ...grpc.CallOption) (*InsertOrderResponse, error)
}

type orderServiceClient struct {
	cc *grpc.ClientConn
}

func NewOrderServiceClient(cc *grpc.ClientConn) OrderServiceClient {
	return &orderServiceClient{cc}
}

func (c *orderServiceClient) GetOrder(ctx context.Context, in *GetOrderRequest, opts ...grpc.CallOption) (*GetOrderResponse, error) {
	out := new(GetOrderResponse)
	err := c.cc.Invoke(ctx, "/orders.OrderService/GetOrder", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *orderServiceClient) UpdateOrder(ctx context.Context, in *UpdateOrderRequest, opts ...grpc.CallOption) (*UpdateOrderResponse, error) {
	out := new(UpdateOrderResponse)
	err := c.cc.Invoke(ctx, "/orders.OrderService/UpdateOrder", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *orderServiceClient) InsertOrder(ctx context.Context, in *InsertOrderRequest, opts ...grpc.CallOption) (*InsertOrderResponse, error) {
	out := new(InsertOrderResponse)
	err := c.cc.Invoke(ctx, "/orders.OrderService/InsertOrder", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// OrderServiceServer is the server API for OrderService service.
type OrderServiceServer interface {
	GetOrder(context.Context, *GetOrderRequest) (*GetOrderResponse, error)
	UpdateOrder(context.Context, *UpdateOrderRequest) (*UpdateOrderResponse, error)
	InsertOrder(context.Context, *InsertOrderRequest) (*InsertOrderResponse, error)
}

// UnimplementedOrderServiceServer can be embedded to have forward compatible implementations.
type UnimplementedOrderServiceServer struct {
}

func (*UnimplementedOrderServiceServer) GetOrder(ctx context.Context, req *GetOrderRequest) (*GetOrderResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetOrder not implemented")
}
func (*UnimplementedOrderServiceServer) UpdateOrder(ctx context.Context, req *UpdateOrderRequest) (*UpdateOrderResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateOrder not implemented")
}
func (*UnimplementedOrderServiceServer) InsertOrder(ctx context.Context, req *InsertOrderRequest) (*InsertOrderResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method InsertOrder not implemented")
}

func RegisterOrderServiceServer(s *grpc.Server, srv OrderServiceServer) {
	s.RegisterService(&_OrderService_serviceDesc, srv)
}

func _OrderService_GetOrder_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetOrderRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OrderServiceServer).GetOrder(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/orders.OrderService/GetOrder",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OrderServiceServer).GetOrder(ctx, req.(*GetOrderRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _OrderService_UpdateOrder_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateOrderRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OrderServiceServer).UpdateOrder(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/orders.OrderService/UpdateOrder",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OrderServiceServer).UpdateOrder(ctx, req.(*UpdateOrderRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _OrderService_InsertOrder_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(InsertOrderRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OrderServiceServer).InsertOrder(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/orders.OrderService/InsertOrder",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OrderServiceServer).InsertOrder(ctx, req.(*InsertOrderRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _OrderService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "orders.OrderService",
	HandlerType: (*OrderServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetOrder",
			Handler:    _OrderService_GetOrder_Handler,
		},
		{
			MethodName: "UpdateOrder",
			Handler:    _OrderService_UpdateOrder_Handler,
		},
		{
			MethodName: "InsertOrder",
			Handler:    _OrderService_InsertOrder_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "protos/orders/order.proto",
}
