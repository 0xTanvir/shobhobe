package main

import api "gitlab.com/0xTanvir/shobhobe/internal/api/users"

func main() {
	app := api.NewApp()
	app.Init()
}
