# ShobHobe Store Prototype

### Setup
1. [Download](https://golang.org/dl/) Go (minimum) version 1.12.* 
2. Run `scripts/setup.sh`
3. Run `mage -l` for a list of commands
```
tanvir@playground:~/code/src/gitlab.com/0xTanvir/shobhobe (master)$ mage -l
Targets:
  lint                  Run lint checks
  protoc:orders         generate order client for go
  protoc:products       generate product client for go
  protoc:users          generate user client for go
  run:orders            Run the order service binary
  run:ordersClient      Run the order client service binary
  run:products          Run the product service binary
  run:productsClient    Run the product client service binary
  run:users             Run the user service binary
  run:usersClient       Run the user client service binary
  test:all              Runs all tests
tanvir@playground:~/code/src/gitlab.com/0xTanvir/shobhobe (master)$
```

### Tools
 * [Mage](https://github.com/magefile/mage) build tool.
 * [Logrus](https://github.com/sirupsen/logrus) for logging.
 * [GolangCILint](https://github.com/golangci/golangci-lint) linter.
 * [Goconvey](https://github.com/smartystreets/goconvey) Test framework.
 * [gRPC](https://github.com/grpc/grpc-go) gRPC implementation of go.

## Overview

#### ./build
contain all the build file for this project

#### ./cmd
that's where start the execution of a service

#### ./config
contains all the configuration info for the project

#### ./internal
api and other internal file for the project

#### ./pkg
contain all the package of the project

#### ./protos
contain all the protobuf file

#### ./repo
contain any repository related file

#### ./scripts
contain any script for the project

#### ./tests
contain all the unit test of the project