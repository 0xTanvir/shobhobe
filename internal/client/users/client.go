package main

import (
	"context"
	"fmt"

	"github.com/sirupsen/logrus"
	proto "gitlab.com/0xTanvir/shobhobe/protos/users"
	"google.golang.org/grpc"
)

func main() {
	logrus.Info("Users Client Service Started")

	opts := grpc.WithInsecure()

	cc, err := grpc.Dial("localhost:50051", opts)
	if err != nil {
		logrus.Fatalf("could not connect: %v", err)
	}
	defer cc.Close()

	c := proto.NewUserServiceClient(cc)

	// **************
	// Inserting user
	// **************
	// user := &proto.InsertUserRequest{
	// 	User: &proto.User{
	// 		Id:       "xxx",
	// 		Name:     "Fake Name",
	// 		Email:    "fake@fake.com",
	// 		Username: "fake-username",
	// 	},
	// 	Password: "fake-password",
	// }

	// rs, err := c.InsertUser(context.Background(), user)
	// if err != nil {
	// 	logrus.Fatalf("Error while inserting user: %v\n", err)
	// }
	// fmt.Printf("insert user response: %v\n", *rs)

	// *************
	// Updating user
	// *************
	patch := &proto.Patch{
		Op:    "replace",
		Path:  "/name",
		Value: "Patch Name",
	}
	patchs := []*proto.Patch{patch}
	uUser := &proto.UpdateUserRequest{
		Id:        "xxx",
		JsonPatch: patchs,
	}

	rss, err := c.UpdateUser(context.Background(), uUser)
	if err != nil {
		logrus.Fatalf("Error while getting user: %v\n", err)
	}
	fmt.Printf("getting user response: %v\n", rss.Result)

	// ************
	// Getting user
	// ************
	gUser := &proto.GetUserRequest{Id: "xxx"}
	rs, err := c.GetUser(context.Background(), gUser)
	if err != nil {
		logrus.Fatalf("Error while getting user: %v\n", err)
	}
	fmt.Printf("getting user response: %v\n", *rs.User)

}
