package products

import (
	"context"
	"encoding/json"

	svc "gitlab.com/0xTanvir/shobhobe/pkg/svc/products"
	proto "gitlab.com/0xTanvir/shobhobe/protos/products"
)

// Controller is the products controller
type Controller struct {
	Service *svc.Service
}

// InsertProduct add product
func (c *Controller) InsertProduct(ctx context.Context, req *proto.InsertProductRequest) (*proto.InsertProductResponse, error) {

	product, err := c.Service.InsertProduct(req.Product)
	if err != nil {
		return nil, err
	}

	return &proto.InsertProductResponse{Product: product}, nil
}

// UpdateProduct update product
func (c *Controller) UpdateProduct(ctx context.Context, req *proto.UpdateProductRequest) (*proto.UpdateProductResponse, error) {

	b, err := json.Marshal(req.JsonPatch)
	if err != nil {
		return nil, err
	}

	return c.Service.UpdateProduct(req.Id, b)
}

// GetProduct get product
func (c *Controller) GetProduct(ctx context.Context, req *proto.GetProductRequest) (*proto.GetProductResponse, error) {
	product, err := c.Service.GetProduct(req.Id)
	if err != nil {
		return nil, err
	}

	return &proto.GetProductResponse{Product: product}, nil
}
