package orders

import (
	"context"
	"encoding/json"

	svc "gitlab.com/0xTanvir/shobhobe/pkg/svc/orders"
	proto "gitlab.com/0xTanvir/shobhobe/protos/orders"
)

// Controller is the orders controller
type Controller struct {
	Service *svc.Service
}

// InsertOrder add an order
func (c *Controller) InsertOrder(ctx context.Context, req *proto.InsertOrderRequest) (*proto.InsertOrderResponse, error) {

	order, err := c.Service.InsertOrder(req.Order)
	if err != nil {
		return nil, err
	}

	return &proto.InsertOrderResponse{Order: order}, nil
}

// UpdateOrder update an order
func (c *Controller) UpdateOrder(ctx context.Context, req *proto.UpdateOrderRequest) (*proto.UpdateOrderResponse, error) {

	b, err := json.Marshal(req.JsonPatch)
	if err != nil {
		return nil, err
	}

	return c.Service.UpdateOrder(req.Id, b)
}

// GetOrder get an order
func (c *Controller) GetOrder(ctx context.Context, req *proto.GetOrderRequest) (*proto.GetOrderResponse, error) {
	order, err := c.Service.GetOrder(req.Id)
	if err != nil {
		return nil, err
	}

	return &proto.GetOrderResponse{Order: order}, nil
}
