package users

import (
	"flag"
	"net"
	"os"
	"os/signal"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/0xTanvir/shobhobe/config"
	svc "gitlab.com/0xTanvir/shobhobe/pkg/svc/users"
	proto "gitlab.com/0xTanvir/shobhobe/protos/users"
	"gitlab.com/0xTanvir/shobhobe/repo"
	"google.golang.org/grpc"
)

var (
	// cfgName config file name
	cfgName string
	// cfgPaths config file path
	cfgPaths string
)

// App is app struct for users
type App struct {
}

// NewApp create new users app
func NewApp() *App {
	return &App{}
}

// Init initialize users app router
func (api *App) Init() {
	setupRoutes()
}

func parsePort() string {
	port := os.Getenv("USERSVC_PORT")
	if len(port) < 1 {
		port = "50051"
	}
	return "0.0.0.0:" + port
}

// init initializes flags.
func init() {
	flag.StringVar(&cfgName, "cfg-name", "dev", "config file name without path and extension")
	flag.StringVar(&cfgPaths, "cfg-paths", "./config", "paths where we search config split them by ','")
}

func setupRoutes() {
	logrus.Info("Users Service Started")

	listenPort := parsePort()

	// Load config with viper
	if err := config.Load(cfgName, strings.Split(cfgPaths, ",")...); err != nil {
		logrus.Panic(err.Error())
	}
	logrus.Infof("loaded config: %v", viper.ConfigFileUsed())

	// Start the server
	lis, err := net.Listen("tcp", listenPort)
	if err != nil {
		logrus.Fatalf("failed to listen: %v", err)
	} else {
		logrus.Info("Users is listening to: ", listenPort)
	}

	opts := []grpc.ServerOption{}
	s := grpc.NewServer(opts...)

	pool := repo.GetRedisPool()

	proto.RegisterUserServiceServer(s, &Controller{Service: &svc.Service{Pool: pool}})

	go func() {
		logrus.Info("Server is running...")
		if err := s.Serve(lis); err != nil {
			logrus.Fatalf("failed to serve: %v", err)
		} else {
			logrus.Infoln("Users listening to: ", listenPort)
		}
	}()

	// Wait for signal intruption (Control+C) to exit
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)

	// Block until a signal is received
	<-ch
	logrus.Info("Stopping the server")
	s.Stop()
	logrus.Info("Closing the listener")
	lis.Close()
	logrus.Info("End of Program")
}
