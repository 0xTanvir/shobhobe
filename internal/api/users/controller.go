package users

import (
	"context"
	"encoding/json"

	"gitlab.com/0xTanvir/shobhobe/pkg/models"
	svc "gitlab.com/0xTanvir/shobhobe/pkg/svc/users"
	proto "gitlab.com/0xTanvir/shobhobe/protos/users"
)

// Controller is the users controller
type Controller struct {
	Service *svc.Service
}

// InsertUser add user
func (c *Controller) InsertUser(ctx context.Context, req *proto.InsertUserRequest) (*proto.InsertUserResponse, error) {

	user, err := c.Service.InsertUser(&models.User{
		ID:       req.User.Id,
		Name:     req.User.Name,
		Email:    req.User.Email,
		Username: req.User.Username,
		Password: req.Password,
	})
	if err != nil {
		return nil, err
	}

	return &proto.InsertUserResponse{User: user}, nil
}

// UpdateUser update user
func (c *Controller) UpdateUser(ctx context.Context, req *proto.UpdateUserRequest) (*proto.UpdateUserResponse, error) {

	b, err := json.Marshal(req.JsonPatch)
	if err != nil {
		return nil, err
	}

	return c.Service.UpdateUser(req.Id, b)
}

// GetUser get user
func (c *Controller) GetUser(ctx context.Context, req *proto.GetUserRequest) (*proto.GetUserResponse, error) {
	user, err := c.Service.GetUser(req.Id)
	if err != nil {
		return nil, err
	}

	return &proto.GetUserResponse{User: user}, nil
}
