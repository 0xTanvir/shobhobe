package users

import (
	"fmt"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
)

// Run type of name space
type Run mg.Namespace

// Users Run the user service binary
func (Run) Users() error {
	fmt.Println("... Running user binary")
	return sh.RunV("go", "run", "gitlab.com/0xTanvir/shobhobe/cmd/users")
}

// UsersClient Run the user client service binary
func (Run) UsersClient() error {
	fmt.Println("... Running user client binary")
	return sh.RunV("go", "run", "gitlab.com/0xTanvir/shobhobe/internal/client/users")
}
