package orders

import (
	"fmt"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
)

// Run type of name space
type Run mg.Namespace

// Orders Run the order service binary
func (Run) Orders() error {
	fmt.Println("... Running order binary")
	return sh.RunV("go", "run", "gitlab.com/0xTanvir/shobhobe/cmd/orders")
}

// OrdersClient Run the order client service binary
func (Run) OrdersClient() error {
	fmt.Println("... Running order client binary")
	return sh.RunV("go", "run", "gitlab.com/0xTanvir/shobhobe/internal/client/orders")
}
