package proto

import (
	"fmt"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
)

// Protoc type of name space
type Protoc mg.Namespace

// Users generate user client for go
func (Protoc) Users() error {
	fmt.Println("... Generating user")
	return sh.RunV("protoc", "protos/users/user.proto", "--go_out=plugins=grpc:.")
}

// Products generate product client for go
func (Protoc) Products() error {
	fmt.Println("... Generating product")
	return sh.RunV("protoc", "protos/products/product.proto", "--go_out=plugins=grpc:.")
}

// Orders generate order client for go
func (Protoc) Orders() error {
	fmt.Println("... Generating order")
	return sh.RunV("protoc", "protos/orders/order.proto", "--go_out=plugins=grpc:.")
}
