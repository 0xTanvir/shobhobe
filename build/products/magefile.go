package products

import (
	"fmt"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
)

// Run type of name space
type Run mg.Namespace

// Products Run the product service binary
func (Run) Products() error {
	fmt.Println("... Running product binary")
	return sh.RunV("go", "run", "gitlab.com/0xTanvir/shobhobe/cmd/products")
}

// ProductsClient Run the product client service binary
func (Run) ProductsClient() error {
	fmt.Println("... Running product client binary")
	return sh.RunV("go", "run", "gitlab.com/0xTanvir/shobhobe/internal/client/products")
}
