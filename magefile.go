//+build mage

package main

import (
	"fmt"
	"os"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"

	// mage:import
	_ "gitlab.com/0xTanvir/shobhobe/build/proto"
	// mage:import
	_ "gitlab.com/0xTanvir/shobhobe/build/users"
	// mage:import
	_ "gitlab.com/0xTanvir/shobhobe/build/products"
	// mage:import
	_ "gitlab.com/0xTanvir/shobhobe/build/orders"
)

type Test mg.Namespace

func init() {
	os.Setenv("GO111MODULE", "on")
	os.Setenv("CGO_ENABLED", "0")
}

// Runs all tests
func (Test) All() error {
	fmt.Println("... running all tests")
	os.Setenv("ENVIRONMENT", "test")
	return sh.RunV("go", "test", "-v", "-short", "./...")
}

// Run lint checks
func Lint() error {
	fmt.Println("... running  lint checks")
	return sh.RunV("golangci-lint", "run", "-v")
}
