package models

// User struct
type User struct {
	ID       string `json:"id,omitempty"`
	Name     string `json:"name,omitempty"`
	Email    string `json:"eamil,omitempty"`
	Username string `json:"username,omitempty"`
	Password string `json:"password,omitempty"`
}
