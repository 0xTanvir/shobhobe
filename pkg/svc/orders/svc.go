package order

import (
	"encoding/json"
	"fmt"

	jsonpatch "github.com/evanphx/json-patch"
	"github.com/gomodule/redigo/redis"
	"github.com/sirupsen/logrus"
	proto "gitlab.com/0xTanvir/shobhobe/protos/orders"
)

const orderPrefix string = "orders:"

// Service is order service
type Service struct {
	Pool *redis.Pool
}

// InsertOrder insert an order to redis server
func (s *Service) InsertOrder(order *proto.Order) (*proto.Order, error) {

	c := s.Pool.Get()
	defer c.Close()

	json, err := json.Marshal(order)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	// SET order
	_, err = c.Do("SET", orderPrefix+order.Id, json)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return order, nil
}

// UpdateOrder update an order by id
func (s *Service) UpdateOrder(id string, patchJSON []byte) (*proto.UpdateOrderResponse, error) {

	c := s.Pool.Get()
	defer c.Close()

	orderStr, err := redis.String(c.Do("GET", orderPrefix+id))
	if err == redis.ErrNil {
		logrus.Error("Order does not exist")
		return nil, fmt.Errorf("Order does not exist")
	} else if err != nil {
		logrus.Error(err)
		return nil, err
	}

	patch, err := jsonpatch.DecodePatch(patchJSON)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	modified, err := patch.Apply([]byte(orderStr))
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	// SET order
	_, err = c.Do("SET", orderPrefix+id, modified)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return &proto.UpdateOrderResponse{Result: "successfully patched"}, nil
}

// GetOrder get an order by id
func (s *Service) GetOrder(id string) (*proto.Order, error) {

	c := s.Pool.Get()
	defer c.Close()

	orderStr, err := redis.String(c.Do("GET", orderPrefix+id))
	if err == redis.ErrNil {
		logrus.Error("Order does not exist")
		return nil, fmt.Errorf("Order does not exist")
	} else if err != nil {
		logrus.Error(err)
		return nil, err
	}

	order := proto.Order{}
	err = json.Unmarshal([]byte(orderStr), &order)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return &order, nil
}
