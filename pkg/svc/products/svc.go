package product

import (
	"encoding/json"
	"fmt"

	jsonpatch "github.com/evanphx/json-patch"
	"github.com/gomodule/redigo/redis"
	"github.com/sirupsen/logrus"
	proto "gitlab.com/0xTanvir/shobhobe/protos/products"
)

const productPrefix string = "products:"

// Service is product service
type Service struct {
	Pool *redis.Pool
}

// InsertProduct insert a Product to redis server
func (s *Service) InsertProduct(product *proto.Product) (*proto.Product, error) {

	c := s.Pool.Get()
	defer c.Close()

	json, err := json.Marshal(product)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	// SET product
	_, err = c.Do("SET", productPrefix+product.Id, json)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return product, nil
}

// UpdateProduct update a product by id
func (s *Service) UpdateProduct(id string, patchJSON []byte) (*proto.UpdateProductResponse, error) {

	c := s.Pool.Get()
	defer c.Close()

	productStr, err := redis.String(c.Do("GET", productPrefix+id))
	if err == redis.ErrNil {
		logrus.Error("Product does not exist")
		return nil, fmt.Errorf("Product does not exist")
	} else if err != nil {
		logrus.Error(err)
		return nil, err
	}

	patch, err := jsonpatch.DecodePatch(patchJSON)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	modified, err := patch.Apply([]byte(productStr))
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	// SET product
	_, err = c.Do("SET", productPrefix+id, modified)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return &proto.UpdateProductResponse{Result: "successfully patched"}, nil
}

// GetProduct get a product by id
func (s *Service) GetProduct(id string) (*proto.Product, error) {

	c := s.Pool.Get()
	defer c.Close()

	productStr, err := redis.String(c.Do("GET", productPrefix+id))
	if err == redis.ErrNil {
		logrus.Error("Product does not exist")
		return nil, fmt.Errorf("Product does not exist")
	} else if err != nil {
		logrus.Error(err)
		return nil, err
	}

	product := proto.Product{}
	err = json.Unmarshal([]byte(productStr), &product)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return &product, nil
}
