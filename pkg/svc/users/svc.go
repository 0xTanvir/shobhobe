package user

import (
	"encoding/json"
	"fmt"

	jsonpatch "github.com/evanphx/json-patch"
	"github.com/gomodule/redigo/redis"
	"github.com/sirupsen/logrus"
	"gitlab.com/0xTanvir/shobhobe/pkg/models"
	proto "gitlab.com/0xTanvir/shobhobe/protos/users"
)

const userPrefix string = "users:"

// Service is user service
type Service struct {
	Pool *redis.Pool
}

// InsertUser insert an user to redis server
func (s *Service) InsertUser(user *models.User) (*proto.User, error) {

	c := s.Pool.Get()
	defer c.Close()

	json, err := json.Marshal(user)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	// SET user
	_, err = c.Do("SET", userPrefix+user.ID, json)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return &proto.User{
		Id:       user.ID,
		Name:     user.Name,
		Email:    user.Email,
		Username: user.Username,
	}, nil
}

// UpdateUser update an user by id
func (s *Service) UpdateUser(id string, patchJSON []byte) (*proto.UpdateUserResponse, error) {

	c := s.Pool.Get()
	defer c.Close()

	userStr, err := redis.String(c.Do("GET", userPrefix+id))
	if err == redis.ErrNil {
		logrus.Error("User does not exist")
		return nil, fmt.Errorf("User does not exist")
	} else if err != nil {
		logrus.Error(err)
		return nil, err
	}

	patch, err := jsonpatch.DecodePatch(patchJSON)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	modified, err := patch.Apply([]byte(userStr))
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	// SET user
	_, err = c.Do("SET", userPrefix+id, modified)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return &proto.UpdateUserResponse{Result: "successfully patched"}, nil
}

// GetUser get an user by id
func (s *Service) GetUser(id string) (*proto.User, error) {

	c := s.Pool.Get()
	defer c.Close()

	userStr, err := redis.String(c.Do("GET", userPrefix+id))
	if err == redis.ErrNil {
		logrus.Error("User does not exist")
		return nil, fmt.Errorf("User does not exist")
	} else if err != nil {
		logrus.Error(err)
		return nil, err
	}

	user := proto.User{}
	err = json.Unmarshal([]byte(userStr), &user)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return &user, nil
}
