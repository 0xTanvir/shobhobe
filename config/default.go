package config

import (
	"strings"

	"github.com/spf13/viper"
)

// if failed to load the config file it will automatically set these default value
func init() {
	// Allow config via environment variables
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.SetEnvPrefix("SHOB")

	viper.SetDefault("redis.host", "localhost")
	viper.SetDefault("redis.port", "6379")
	viper.SetDefault("redis.maxIdle", 10)
}
