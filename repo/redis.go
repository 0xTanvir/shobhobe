package repo

import (
	"fmt"
	"time"

	"github.com/gomodule/redigo/redis"
	"github.com/spf13/viper"
)

// GetRedisPool create a new redis client
func GetRedisPool() *redis.Pool {
	return &redis.Pool{
		MaxIdle:     viper.GetInt("redis.maxIdle"),
		IdleTimeout: 240 * time.Second,
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", fmt.Sprintf("%s:%s", viper.GetString("redis.host"), viper.GetString("redis.port")))
		},
	}
}

// GetPing tests connectivity for redis (PONG should be returned)
func GetPing(c redis.Conn) error {
	_, err := redis.String(c.Do("PING"))
	if err != nil {
		return err
	}

	return nil
}
