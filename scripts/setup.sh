#!/bin/bash

# Setup Mage
git clone https://github.com/magefile/mage
cd $GOPATH/src/github.com/magefile/mage
go run bootstrap.go

# Setup metalinter
curl -sfL https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | sh -s -- -b $(go env GOPATH)/bin v1.21.0
