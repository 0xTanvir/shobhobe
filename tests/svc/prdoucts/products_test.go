package tests_test

import (
	"context"
	"log"
	"net"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	api "gitlab.com/0xTanvir/shobhobe/internal/api/products"
	svc "gitlab.com/0xTanvir/shobhobe/pkg/svc/products"
	proto "gitlab.com/0xTanvir/shobhobe/protos/products"
	"gitlab.com/0xTanvir/shobhobe/repo"
	"google.golang.org/grpc"
	"google.golang.org/grpc/test/bufconn"
)

const bufSize = 1024 * 1024

var lis *bufconn.Listener

func init() {
	lis = bufconn.Listen(bufSize)
	opts := []grpc.ServerOption{}
	s := grpc.NewServer(opts...)

	pool := repo.GetRedisPool()

	proto.RegisterProductServiceServer(s, &api.Controller{Service: &svc.Service{Pool: pool}})
	go func() {
		if err := s.Serve(lis); err != nil {
			log.Fatalf("Server exited with error: %v", err)
		}
	}()
}

func getBufDialer(listener *bufconn.Listener) func(context.Context, string) (net.Conn, error) {
	return func(ctx context.Context, url string) (net.Conn, error) {
		return listener.Dial()
	}
}

func TestProduct(t *testing.T) {

	ctx := context.Background()

	conn, err := grpc.DialContext(ctx, "bufnet", grpc.WithContextDialer(getBufDialer(lis)), grpc.WithInsecure())
	if err != nil {
		t.Fatalf("Failed to dial bufnet: %v", err)
	}
	defer conn.Close()

	c := proto.NewProductServiceClient(conn)
	product := &proto.InsertProductRequest{
		Product: &proto.Product{
			Id:       "prod-1",
			Name:     "Fake Product",
			Quantity: 3,
			Price: 50,
			Owner: &proto.Owner{Id:"xxx",Name:"Fake Owner"},
		},
	}

	patch := &proto.Patch{
		Op:    "replace",
		Path:  "/name",
		Value: "Fake patch Name",
	}
	patchs := []*proto.Patch{patch}

	Convey("Given new product", t, func() {
		Convey("it should return newly created product without error", func() {
			pdt, err := c.InsertProduct(context.Background(), product)
			So(err, ShouldBeNil)
			So(pdt.Product, ShouldNotBeNil)
		})

	})

	Convey("Given a product id", t, func() {
		Convey("it should return a product without error", func() {
			pdt, err := c.GetProduct(context.Background(), &proto.GetProductRequest{Id: "prod-1"})
			So(err, ShouldBeNil)
			So(pdt.Product, ShouldNotBeNil)
		})
	})

	Convey("Given a product id and patch", t, func() {
		Convey("it should patch the product without error", func() {
			pdt, err := c.UpdateProduct(context.Background(), &proto.UpdateProductRequest{Id: "prod-1",
				JsonPatch: patchs,
			})
			So(err, ShouldBeNil)
			So(pdt.Result, ShouldNotBeEmpty)
		})
	})
}
