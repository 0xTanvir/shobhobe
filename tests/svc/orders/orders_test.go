package tests_test

import (
	"context"
	"log"
	"net"
	"testing"
	"time"

	"github.com/golang/protobuf/ptypes"

	. "github.com/smartystreets/goconvey/convey"
	api "gitlab.com/0xTanvir/shobhobe/internal/api/orders"
	svc "gitlab.com/0xTanvir/shobhobe/pkg/svc/orders"
	proto "gitlab.com/0xTanvir/shobhobe/protos/orders"
	"gitlab.com/0xTanvir/shobhobe/repo"
	"google.golang.org/grpc"
	"google.golang.org/grpc/test/bufconn"
)

const bufSize = 1024 * 1024

var lis *bufconn.Listener

func init() {
	lis = bufconn.Listen(bufSize)
	opts := []grpc.ServerOption{}
	s := grpc.NewServer(opts...)

	pool := repo.GetRedisPool()

	proto.RegisterOrderServiceServer(s, &api.Controller{Service: &svc.Service{Pool: pool}})
	go func() {
		if err := s.Serve(lis); err != nil {
			log.Fatalf("Server exited with error: %v", err)
		}
	}()
}

func getBufDialer(listener *bufconn.Listener) func(context.Context, string) (net.Conn, error) {
	return func(ctx context.Context, url string) (net.Conn, error) {
		return listener.Dial()
	}
}

func TestActivity(t *testing.T) {

	ctx := context.Background()

	conn, err := grpc.DialContext(ctx, "bufnet", grpc.WithContextDialer(getBufDialer(lis)), grpc.WithInsecure())
	if err != nil {
		t.Fatalf("Failed to dial bufnet: %v", err)
	}
	defer conn.Close()

	pt, _ := ptypes.TimestampProto(time.Now())

	c := proto.NewOrderServiceClient(conn)
	order := &proto.InsertOrderRequest{
		Order: &proto.Order{
			Id:          "order-1",
			ProductInfo: &proto.ProductInfo{Id: "prod-1", Name: "Fake Ordr", Quantity: 1, Price: 50},
			User:        &proto.User{Id: "xx1", Name: "Fake Name1", Email: "fake1@fake.com"},
			Date:        pt,
			Owner:       &proto.User{Id: "xx1", Name: "Fake Name1", Email: "fake1@fake.com"},
		},
	}

	patch := &proto.Patch{
		Op:    "replace",
		Path:  "/user/name",
		Value: "Fake patch Name",
	}
	patchs := []*proto.Patch{patch}

	Convey("Given new order", t, func() {
		Convey("it should return newly created order without error", func() {
			o, err := c.InsertOrder(context.Background(), order)
			So(err, ShouldBeNil)
			So(o.Order, ShouldNotBeNil)
		})

	})

	Convey("Given a order id", t, func() {
		Convey("it should return a order without error", func() {
			o, err := c.GetOrder(context.Background(), &proto.GetOrderRequest{Id: "order-1"})
			So(err, ShouldBeNil)
			So(o.Order, ShouldNotBeNil)
		})
	})

	Convey("Given a order id and ptch", t, func() {
		Convey("it should patch the order without error", func() {
			o, err := c.UpdateOrder(context.Background(), &proto.UpdateOrderRequest{Id: "order-1",
				JsonPatch: patchs,
			})
			So(err, ShouldBeNil)
			So(o.Result, ShouldNotBeEmpty)
		})
	})
}
