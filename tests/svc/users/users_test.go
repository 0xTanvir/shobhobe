package tests_test

import (
	"context"
	"log"
	"net"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	api "gitlab.com/0xTanvir/shobhobe/internal/api/users"
	svc "gitlab.com/0xTanvir/shobhobe/pkg/svc/users"
	proto "gitlab.com/0xTanvir/shobhobe/protos/users"
	"gitlab.com/0xTanvir/shobhobe/repo"
	"google.golang.org/grpc"
	"google.golang.org/grpc/test/bufconn"
)

const bufSize = 1024 * 1024

var lis *bufconn.Listener

func init() {
	lis = bufconn.Listen(bufSize)
	opts := []grpc.ServerOption{}
	s := grpc.NewServer(opts...)

	pool := repo.GetRedisPool()

	proto.RegisterUserServiceServer(s, &api.Controller{Service: &svc.Service{Pool: pool}})
	go func() {
		if err := s.Serve(lis); err != nil {
			log.Fatalf("Server exited with error: %v", err)
		}
	}()
}

func getBufDialer(listener *bufconn.Listener) func(context.Context, string) (net.Conn, error) {
	return func(ctx context.Context, url string) (net.Conn, error) {
		return listener.Dial()
	}
}

func TestUser(t *testing.T) {

	ctx := context.Background()

	conn, err := grpc.DialContext(ctx, "bufnet", grpc.WithContextDialer(getBufDialer(lis)), grpc.WithInsecure())
	if err != nil {
		t.Fatalf("Failed to dial bufnet: %v", err)
	}
	defer conn.Close()

	c := proto.NewUserServiceClient(conn)
	user := &proto.InsertUserRequest{
		User: &proto.User{
			Id:       "xx1",
			Name:     "Fake Name1",
			Email:    "fake1@fake.com",
			Username: "fake-username",
		},
		Password: "fake-password",
	}

	patch := &proto.Patch{
		Op:    "replace",
		Path:  "/name",
		Value: "Fake Name",
	}
	patchs := []*proto.Patch{patch}

	Convey("Given new user", t, func() {
		Convey("it should return newly created user without error", func() {
			usr, err := c.InsertUser(context.Background(), user)
			So(err, ShouldBeNil)
			So(usr.User, ShouldNotBeNil)
		})

	})

	Convey("Given a user id", t, func() {
		Convey("it should return a user without error", func() {
			usr, err := c.GetUser(context.Background(), &proto.GetUserRequest{Id: "xx1"})
			So(err, ShouldBeNil)
			So(usr.User, ShouldNotBeNil)
		})
	})

	Convey("Given a user id and patch", t, func() {
		Convey("it should patch the user without error", func() {
			usr, err := c.UpdateUser(context.Background(), &proto.UpdateUserRequest{Id: "xx1",
				JsonPatch: patchs,
			})
			So(err, ShouldBeNil)
			So(usr.Result, ShouldNotBeEmpty)
		})
	})
}
